# common-functions

[![pipeline status](https://gitlab.com/HallMatthew314/common-functions/badges/master/pipeline.svg)](https://gitlab.com/HallMatthew314/common-functions/-/commits/master)

Simple Ruby program. This repo exists for learning CI/CD.

# Build Docker test image

From the root of the repo:

```
docker build -t registry.gitlab.com/hallmatthew314/common-functions -f docker/ruby/Dockerfile .
```

