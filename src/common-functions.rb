#!/usr/bin/env ruby
# encoding: utf-8

require_relative "fibonacci.rb"
require_relative "square.rb"

STRATEGY_INDEX = 0
NUMBER_INDEX = 1

class Calculator

  attr_accessor :strategy, :number

  def initialize(strategy, number)
    @strategy = strategy
    @number = number
  end

  def context
    @strategy.calculate(@number)
  end
end

def main
  strats = {
    "f" => Fibonacci.new,
    "s" => Square.new
  }

  strategy = strats[ARGV[STRATEGY_INDEX]]
  number = ARGV[NUMBER_INDEX].to_i

  c = Calculator.new(strategy, number)

  puts c.context
end

main

