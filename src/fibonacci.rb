# encoding: utf-8

class Fibonacci

  def initialize
    @f = ->(n) { n > 2 ? @f.call(n - 1) + @f.call(n - 2) : 1 }
  end

  def calculate(number)
    unless number.is_a?(Integer)
      if number.is_a?(Float) && number % 1 == 0
        number = number.to_i
      else
        raise ArgumentError, "Expected an integer, got #{number.class}"
      end
    end

    unless number > 0
      raise ArgumentError, "Can't calculate fibonacci numbers less than 1."
    end

    @f.call(number)
  end
end

