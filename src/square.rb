# encoding: utf-8

class Square

  def calculate(number)
    unless number.is_a?(Numeric)
      raise ArgumentError, "Expected a number, got #{number.class}"
    end

    number**2
  end
end 

