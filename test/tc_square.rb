#!/usr/bin/env ruby
# encoding: utf-8

require "test/unit"
require_relative "../src/square.rb"

class TestSquare < Test::Unit::TestCase

  def setup
    @s = Square.new
  end

  def teardown
    @s = nil
  end

  def test_one
    # 1^2 = 1
    assert_equal(1, @s.calculate(1))
  end

  def test_two
    # 2^2 = 4
    assert_equal(4, @s.calculate(2))
  end

  def test_five
    # 5^2 = 25
    assert_equal(25, @s.calculate(5))
  end

  def test_ten
    # 10^2 = 100
    assert_equal(100, @s.calculate(10))
  end

  def test_zero
    # 0^2 = 0
    assert_equal(0, @s.calculate(0))
  end

  def test_negative
    # (-1)^2 = 1
    # (-2)^2 = 4
    assert_equal(1, @s.calculate(-1))
    assert_equal(4, @s.calculate(-2))
  end

  def test_non_numeric
    # The input has to be a number.
    assert_raise(ArgumentError) { @s.calculate("string") }
  end

  def test_non_integer
    # 0.5^2 = 0.25
    # i^2 = -1
    assert_equal(0.25, @s.calculate(0.5))
    assert_equal(-1, @s.calculate(0+1i))
  end
end

