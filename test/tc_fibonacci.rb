#!/usr/bin/env ruby
# encoding: utf-8

require "test/unit"
require_relative "../src/fibonacci.rb"

class TestFibonacci < Test::Unit::TestCase

  def setup
    @f = Fibonacci.new
  end

  def teardown
    @f = nil
  end

  def test_one
    # The first fibonacci number is 1.
    assert_equal(1, @f.calculate(1))
  end

  def test_two
    # The second fibonacci number is 1.
    assert_equal(1, @f.calculate(2))
  end

  def test_five
    # The fifth fibonacci number is 5.
    assert_equal(5, @f.calculate(5))
  end

  def test_ten
    # The tenth fibonacci number is 55.
    assert_equal(55, @f.calculate(10))
  end

  def test_zero
    # There is no fibonacci number 0.
    assert_raise(ArgumentError) { @f.calculate(0) }
  end

  def test_negative
    # There are no negative fibonacci numbers.
    assert_raise(ArgumentError) { @f.calculate(-1) }
  end

  def test_non_integer
    # The input has to be an Integer.
    assert_raise(ArgumentError) { @f.calculate("string") }
    assert_raise(ArgumentError) { @f.calculate(1.5) }
  end

  def test_float_with_integer_value
    # Floats are allowed if they have a whole number value.
    # e.g. 1.0, 2.0, 9.0, 20.0
    assert_equal(1, @f.calculate(1.0))
    assert_equal(1, @f.calculate(2.0))
    assert_equal(34, @f.calculate(9.0))
    assert_equal(6765, @f.calculate(20.0))
  end
end

